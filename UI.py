from cryptogra import decryp
from getpass import getpass

def UI_login():
    '''
    Se solicita la primera opcion en la navegacion / Ser retorna la opcion
    '''
    print("\nHola bienvenido a este programa para administrar las claves")
    print("Por favor ingrese el numero correspondiente a la opcion que desea ingresar y precione 'Enter':\n")
    print("1) Crear cuenta\n2) Iniciar Sesion\n3) Salir\n")
    opin=input("->")
    if opin == '1' or opin == '2' or opin == '3': #Verifico si la opcion es valida
        op=opin #se guarda con la opcion del siguiente menu
    else:
        op='000' #Si no se envia al posible error
    return op

def UI_input_check(ispassword):
    '''
    Se mantiene en loop hasta que comprueba que un input no este vacio/retorna el string ingresado
    '''
    inputuser=""#Variable que albergara el input del usuario
    if ispassword==False: #Compruebo si se trata de un password
        inputuser=input("->")
    else:
        inputuser=getpass('->')#Si es un password se utiliza un imput que no genera echo
    
    if inputuser=="": #Si el usuario no ingresa nada se envia el error
        print("*Este campo es obligario")
        return UI_input_check(ispassword)#Vuelve a entrar a la funcion hasta que digite almenos un caracter
    else:
        return(inputuser)#Si pasa las verificaciones se envia el valor ingresado

def UI_create():
    '''
    Se solicitan y retornan los datos para crear cuenta
    '''
    datosC=[] #Matriz que guardará los datos
    print("*"*5,"Crear usuario","*"*5)
    print("\nIngrese nombre de usuario")#Se envian mensajes para solicitar el dato
    datosC.append(UI_input_check(False))  # si completa la verificacion se guarda el dato    
    print("\nIngrese contraseña")
    datosC.append(UI_input_check(True))
    print("Ingrese nombres y apellidos\n")
    datosC.append(UI_input_check(False))
    return datosC #Se envia la matriz con todos los datos

def UI_star():
    '''
    Se solicitan y retornan los datos para iniciar sesion
    '''
    datosS=[]# Matriz que albergara los datos
    print("*"*5,"Iniciar sesion","*"*5)
    print("Ingrese nombre de usuario\n")#Se envian mensajes para solicitar el dato
    datosS.append(UI_input_check(False)) # si completa la verificacion se guarda el dato 
    print("Ingrese contraseña\n")
    datosS.append(UI_input_check(True))
    return datosS #Se envia la matriz con todos los datos

def exit(op):
    '''
    Se despide del usuario/Se retorna la opcion que finaliza el programa
    '''
    #Se crean los mensajes que vera el usuario dependiendo de la opcion elegida en el menu
    if op == 0: #Mensaje de despedida al cerrar el programa
        print("\n","*"*5,"Hasta proto :)","*"*5,"\n")
        return '00'#Retorna la opcion que finaliza el programa
    else:#Mensaje de finalizar sesion
        print("\n","*"*5,"Sesion finalizada","*"*5,"\n")
        return '0'#Retorna la opcion que envia al menu inicial del programa

def UI_menu_user():
    '''
    Menu usuario logeado / se rotorna la opcion en el submenu
    '''
    print("\nPor favor ingrese el numero correspondiente\na la opcion que desea ingresar y precione 'Enter':\n")
    print("1) Ver lista de ítems\n2) Ver lista de tags\n3) Cerrar Sesion")
    opMenuU=input("->")     
    if opMenuU == '1' or opMenuU == '2' or opMenuU == '3': #Verifico si la opcion es valida
        op=opMenuU
    else:
        op='000'#Se llena la variable con la opcion que notificara el posible error
    return op

def UI_lstItem_show(lst_items):
    '''
    Presenta la lista de items/Ingresa la lista de items
    '''
    print("\n","*"*5,"Lista de items:","*"*5,"\n")
    if len(lst_items)!= 0: #Verifico si la matriz con la lista de items no esta vacia
        for data in lst_items:
            print("\nID del item: ",data["IID"],"\nTitulo del item: ",data["Titulo"],"\nURL del item: ",data["URL"],"\nComentario: ",data["Comentario"])
    else: 
        print("No hay items") # Si esta vacia se le informa al usuario

def UI_menu_items(lst_items):
    '''
    Se muestra la lista de items y el menu de items / se retorna la opcion del submenu / ingresan la lista de items del usuario logueado
    '''
    UI_lstItem_show(lst_items)
    print("\nPor favor ingrese el numero correspondiente a la opcion que desea ingresar y precione 'Enter':\n")
    print("1) Ver Item\n2) Modificar Item\n3) Eliminar Item\n4) Crear Item\n5) Volver atras\n")
    opMenuI=input("->")     
    if opMenuI == '1':#Se presentan las opciones para el menu de navegacion 
        op='4' #Se guarda la opcion para el menu de navegacion
    elif opMenuI == '2':
        op='5'
    elif opMenuI == '3':
        op='6'
    elif opMenuI == '4':
        op='7'
    elif opMenuI == '5':
        op='0'
    else:
        op='000' #Si no envia un dato de la lista, se envia la opcion que notificara el error
    return op, lst_items

def UI_error_nav(error):
    '''
    Se le informa al usuario del posible error y se envia a la opcion que le perminta continuar/ retorna la opcion para el menu/ ingresa el codigo de error
    '''
    if error =='021': #Se presentan los distintos datos de error
        print("\nERROR: Item no encontrado\n")
        op='1'#Se envia el menu anterior al error
    elif error == '000':
        print('\nERROR: Opcion no valida vuelva a intentarlo\n')
        op='0'
    elif error == '022':
        print('\nERROR: El tag no existe\n')
        op='2'
    elif error == '022-12':
        print('\nERROR: El tag ya existe\n')
        op='2'
    elif error =='02':
        print("\nERROR: Usuario no encontrado\n")
        op='2'
    return op #Se retorna la opcion del menu de navegacion

def UI_item_search():
    '''
    Se solicita y retorna el ID del item
    '''
    print("\nIngrese el ID del item\n")
    IID=input("->") 
    return IID

def UI_item_rmv():
    '''
    Se confirma la eliminacion de un item
    '''
    confirm=input("\nSi esta seguro de continuar escriba la letra 'S' y presione 'Enter'\nSi no escriba la letra 'N' y presione entern\n->")#Se presenta la informacion al usuario
    if confirm =='S' or 's': #Se verifica si el usuario digito un caracter que confirma la accion
        return True #Si entra, se retorna el valor que permite continuar con la modificacion
    elif confirm =='N' or 'n': #Se verifica si el usuario digito un caracter que cancela la accion
        print("Se ha abortado la accion")
        return False
    else: # si el usuario ingresa cualquier otro caracter se le informa y no se continua
        print("No se ingreso un caracter valido accion abortada")
    return False

def UI_item_confirm(datosMod):
    '''
    Se confirma la escritura de los datos en el json/Ingresa matriz de datos/se retorna la matriz vacia o con datos
    '''
    confirm=input("\nSi esta seguro de continuar escriba la letra 'S' y presione 'Enter'\nSi no, escriba la letra 'N' y presione enter\n->")
    if confirm =='S' or confirm =='s':#Si el usuario ingresa el caracter correcto se retorna la matriz con los datos
        return datosMod #se retorna la matriz con los datos
    elif confirm =='N' or confirm =='n': #si el usuario ingresa n
        print("*Se ha abortado la accion")
        datosMod=[]#Se retorna una matiz vacia
        return []
    else:#Se se ingresa cualquier otro caracter
        print("*No se ingreso un caracter valido accion abortada")
        datosMod=[]#Se envia matriz vacia
        return []

def UI_item_lstTags(dat_tag_lst):
    '''
    Se presenta la lista de tags/Ingresa la lista de tags
    '''
    if dat_tag_lst!=[]:#Se verifica si la matriz esta vacia
        print("\n","*"*5,"Lista de tags:","*"*5,"\n")
        for tag in dat_tag_lst:#Se recorre la matriz
            print(tag)#Se imprime cada item de la matriz
    else:#Si esta vacia se le indica al usuario
        print("\n*No hay lista de tags:")


def UI_item_Intag(dat_tag_lst):
    '''
    Se solicitan los tags a agregar al item/ingresa la matriz con los datos ingresados y lista de tags del usuario/Se retorna la matriz con los datos
    '''
    inf="El tag ingresado no esta en la lista"#Variable que guardara el mensaje
    tags=[]#Matiz donde se guarda la lista de tags temporalmente
    op="S"#Variable que hara que se mantenga el loop
    inputuser=(input("\nDigite un tag de la lista de tags creados y que desea asignar a este item, si no desea agregar ningun tag, solo presione 'Enter'\n->"))    
    if inputuser!="": #Comprueba si el usuario no digito nada en tal caso no se agregan tags
        while op=="S" or op=="s": #Se crea un loop que continuara dependiendo de si el usuario desea ingresar mas tags
            exist=False
            for tag in dat_tag_lst: #Recorro la lista de tags del usuario
                for tag_exis in tags:#Recorro la matriz de tags temporal
                    if tag_exis == inputuser:#Verifico que el tag no este repetido
                        exist=True
                if inputuser==tag :#Comparo la entrada del usuario con los tags de su lista
                    if exist == False:
                        tags.append(inputuser)#Si lo encuentra guardo el tag en la matriz temporal
                        inf="\n*Tag agregado"#Se crea el string que informa al usuario
                        break#se rompe el loop para que no se cambie el mensaje
                    else:
                        inf="\n*El tag ya fue agregado a este item"
                        break
                else:
                    inf="\n*El tag ingresado no esta en la lista"
            print(inf)#Se informa al usuario del resultado de la digitacion 
            op=input('\n¿Desea ingresar otro tag?.\nPresione "S" para hacerlo o "N" para continuar\n->')#Se pregunta al usuario si desea ingresar mas tags
            if op=="S" or op=="s":#Dependiendo del valor de op s le informa al usuario
                inputuser=input("\nDigite el tag\n->")
            elif op=="N" or op=="n":
                print("\n*No se agregaron más tags\n")
            else:#se informa al usuario que no se agregaran mas tags
                print("\n*Caracter invalido, no se agregaron más tags\n")
    else:#Si el usuario decide no ingresar nada
        tags.append("Sin Tags")#se envia el valor por defecto
    return tags#Se retorna la matriz

def UI_item_modi(op,dat_tag_lst):
    '''
    Se solicitan y retornan los datos para modificar el item/Ingresa tipo de opcion/Se retorna una matriz
    ''' 
    datosMod=[]#Lista que guardara los datos digitados
    if op == 1: #Verifico si se va a modificar un item o si se va a crear uno nuevo
        print("\n","*"*5,"Modificacion de item","*"*5,"\n")
    elif op == 2:
        print("\n","*"*5,"Creacion de item","*"*5,"\n")
    #Se solisitan los datos obligatorios
    print("Ingrese Titulo")
    datosMod.append(UI_input_check(False))
    print("Ingrese el nombre de usuario")
    datosMod.append(UI_input_check(False))
    print("Ingrese contraseña")
    datosMod.append(UI_input_check(False))
    print("Ingrese el URL")
    datosMod.append(UI_input_check(False))
    #Se solisitan los que no son obligatorios
    datosMod.append(input("Si desea ingresar algun comentario digitelo, de lo contrario presione 'Enter'\n->"))
    UI_item_lstTags(dat_tag_lst)#Se muestra la lista de tags actuales del usuario
    datosMod.append(UI_item_Intag(dat_tag_lst))#Se optiene la matriz con los tags
    return UI_item_confirm(datosMod)


def UI_item_show(lst_items,IID):
    '''
    Se presenta al usuario la informacion del item /Se retorna la opcion del menu
    '''
    op='021'#variable que retornara la siguiente opcion del menu
    for data in lst_items:
        if data["IID"]==IID:#Se busca el id del item
            print("\n","*"*5,"Informacion completa del item","*"*5,"\n")
            print("\nID del item: ",data["IID"],"\nTitulo del item: ",data["Titulo"],"\nNombre de usuario: ",data["Username"],"\nContraseña: ",decryp(data["Password"]),"\nURL del item: ",data["URL"],"\nComentario: ",data["Comentario"],"\nLista de tags: ",data["Tags"])
            op='0'
            break
        else:#si no se encuantra el id
            op='021'#Se muestra el posible error
    return op#Se retorna la opcion

def UI_menu_tags(lst_tags):
    '''
    Se muestra la lista de tags y el menu de tags / se retorna la opcion del submenu y la lista de tags/ ingresan la lista de tags del usuario logueado
    '''
    UI_item_lstTags(lst_tags)#Se llama la funcion que presenta la lista de items
    print("\nPor favor ingrese el numero correspondiente a la opcion que desea ingresar y precione 'Enter':\n")
    print("1) Ver Tag\n2) Modificar Tag\n3) Eliminar Tag\n4) Crear Tag\n5) Volver atras\n")
    opMenuI=input("->")     
    if opMenuI == '1':#Se presentan las opciones para el menu de navegacion 
        op='8'#Se guarda la opcion para el menu de navegacion
    elif opMenuI == '2':
        op='9'
    elif opMenuI == '3':
        op='11'
    elif opMenuI == '4':
        op='12'
    elif opMenuI == '5':
        op='0'
    else:
        op='000'
    return op, lst_tags #Se retorna la opcion del menu y la lista de tags

def UI_tag_in(op):
    '''
    Se solicitan y retornan el nombre del tag/ingresa tipo de opcion
    ''' 
    #Se presentan los titulos de los tags
    if op == 0:#dependiendo de la opcion del menu se muestra un mensaje diferente
        tagToMod=(input("Ingrese el nombre del tag que desea ver\n->"))
    elif op == 1:
        tagToMod=(input("Ingrese el nombre del tag a modificar\n->"))
    elif op==2:
        tagToMod=(input("Ingrese el nombre del tag a elminar\n->"))      
    elif op==3:
        tagToMod=(input("Ingrese el nombre para el nuevo tag\n->"))      
    return tagToMod#se retorna el valor que ingresa el usuario

def UI_tag_title(op):
    '''
    Se presenta al usuario el titulo del apartado/ingresa tipo de opcion
    ''' 
    if op == 1:#Se verifica la opcion del menu al que pertenece
        print("\n","*"*5,"Modificar tag:","*"*5,"\n")
    elif op==2:
        print("\n","*"*5,"Eliminnar tag:","*"*5,"\n")      
    elif op==3:
        print("\n","*"*5,"Creacion de tag:","*"*5,"\n")

def UI_tag_show(tagToMod,lst_items_whitTag):
    '''
    Se mustra al usuario los items asciados al tag/Retorna siguiente opcion/imgresa el tag encontrado en la lista de tags y la lista de items del usuario
    '''
    if tagToMod != []:#Se verifica si la matriz con los datos ingresados por el usuario esta vacia
        print("\n","*"*5,"Ver tag:","*"*5,"\n")
        if lst_items_whitTag != []:#Se verifica si la matriz que indica cuales items tienen el tag esta vacia
            print("Nombre del tag:",tagToMod)#Se muestra el nombre del tag
            for item in lst_items_whitTag:#Se recorre la lista de items con este tag
                print("\nID del item: ",item["IID"],"\nTitulo del item: ",item["Titulo"],"\nNombre de Usuario: ",item["Username"],"\nURL: ",item["URL"])#Se imprimen los datos del item
        else:#si esta vacia se le indica el usuario
            print("Ningun item tiene asignado este tag")
        return '2'
    else:#si esta vacia se envia a la opcion de error
        return '022'