import json
from UI import *
from data import *

#Navegacion por el menu del programa
def nav1(op):
    if op=='0': #MENU PRINCIPAL
        op=UI_login() 
    elif  op == '1': # CREAR USUARIO
        op=dat_create(UI_create(),read(1))
    elif op == '2': # LOGUEO
        UID,op=dat_login(UI_star(),read(1))
        op2='0'#opcion por defecto del submenu de logueo
        while op=='22': #SUB MENU DE USUARIO
            if op2=='0': #PRINCIPAL
                op2=UI_menu_user()
            elif op2 == '1':# SUB MENU DE ITEM
                dat_doc=read(2)
                op2,lst_items_user=UI_menu_items(dat_items_lst(UID,dat_doc)) 
            elif op2 == '2':# SUB MENU DE TAG
                dat_doc=read(2)
                op2,lst_tag_user=UI_menu_tags(dat_tag_lst(UID,dat_doc)) 
            elif op2 == '3':#CERRAR SESION
                op=exit(2)
            #                       ITEMS
            elif op2=='4':#VER INFO COMPLETA DEL ITEM
                op2=UI_item_show(lst_items_user,UI_item_search()) 
            elif op2 == '5':# MODIFICACION DE ITEMS
                item_info=dat_item_search(UI_item_search(),lst_items_user)#guardo la informacion del item en una variable
                if item_info != []:#verifico si esta vacia
                    op2=dat_item_modi(item_info,dat_doc,UI_item_modi(1,dat_tag_lst(UID,dat_doc)))
                else:
                    op2='021'#muestro el error
            elif op2 == '6':# ELIMINACION DE ITEMS
                IID=UI_item_search()#guardo el id del item en una variable 
                item_info=dat_item_search(IID,lst_items_user)#guardo la imformacion del item
                if item_info != []:#verifico si esta vacio
                    if UI_item_rmv() == True:#realizo la confirmacion de la accion
                        op2=dat_item_rmv(IID,dat_doc,lst_items_user)
                else:
                    op2='021'
            elif op2=='7':#CREAR UN NUEVO ITEM
                op2=dat_item_crea(UI_item_modi(2,dat_tag_lst(UID,dat_doc)),lst_items_user,dat_doc,UID)
            #                       TAGS
            elif op2=='8':#VER TAG
                tagToMod=UI_tag_in(0)#creacion de una variable que alberga el dato ingresaodo por el usuario
                tag_found=dat_tag_search(tagToMod,lst_tag_user)#se guarda la matriz de items con ese tag
                op2=UI_tag_show(tag_found,dat_tag_InItem(tag_found,dat_items_lst(UID,dat_doc)))
            elif op2=='9':#MODIFICAR TAG
                UI_tag_title(1)
                tagToMod=UI_tag_in(1)#se solisitan los datos del tag a eliminar
                tag_found=dat_tag_search(tagToMod,lst_tag_user)
                tag_inlist=dat_tag_InItem(tag_found,dat_items_lst(UID,dat_doc))#se guarda el tag que se encuentre en la lista de items
                op2=dat_tag_rmv(tag_found,lst_tag_user,dat_doc,tag_inlist)#se elimina el tag encontrado
                if op2 !='022':#se crea el nuevo tag
                    tagToMod=UI_tag_in(3)#se solicitan los datos del nuevo tag
                    tag_found2=dat_tag_search(tagToMod,lst_tag_user)
                    if tag_found2 == []:
                        op2=dat_tag_crea(tagToMod,lst_tag_user,dat_doc,UID,1,tag_inlist)
                    else:
                        op2=='022-12'
            elif op2=='11':#ELIMINAR TAG
                UI_tag_title(2)
                tagToMod=UI_tag_in(2)
                tag_found=dat_tag_search(tagToMod,lst_tag_user)
                op2=dat_tag_rmv(tag_found,lst_tag_user,dat_doc,dat_tag_InItem(tag_found,dat_items_lst(UID,dat_doc)))
            elif op2=='12':#CREAR TAG
                UI_tag_title(3)
                tagToMod=UI_tag_in(3)
                tag_found=dat_tag_search(tagToMod,lst_tag_user)#se guarda la matriz de items con ese tag
                if tag_found == []:#si esta vacia signofica que no esta repetida
                    op2=dat_tag_crea(tagToMod,lst_tag_user,dat_doc,UID,0,[])
                else:
                    op2==UI_error_nav('022-12')
            else:
                op2=UI_error_nav(op2)

    elif op == '3': #SALIR
        op=exit(0) # opcion que termina con el loop que hace correr el programa
    else:
        op=UI_error_nav(op)
    return op

op ='0'
while op!='00': #Se mantiene en loop el programa
    op=nav1(op)