import json
import time
from cryptogra import *

def read(archivo):
    '''
    Lectura del archivo json/ingresa cual archivo leer 1 para users o 2 para data/retorna los datos del json
    '''
    if archivo ==1: # Dependiendo de la opcion que ingresa se lee el archivo de data o de users
        nombre="users.json"
    elif archivo ==2:
        nombre="data.json"

    doc=open(nombre,"r")#se abre el json y se lee
    dat_doc=json.load(doc)#se guardan los datos en la variable a retornar
    doc.close#se cierra el archivo
    return dat_doc

def dat_create(datosC,dat_doc):
    '''
    Creacion de nuevo usuario/Se retorna la siguiente opcion/ ingresa los datos digitados del usuario y los datos de usuarios guardados en el Json
    '''
    id_user=str(round(time.time()*1000))[-4:] #Creacion de ID de 4 digitos
    password=cryp(datosC[1])# se encripta el dato antes de ser agregado al json
    dat_doc["Users"].append({
    "Username": datosC[0],
    "Password":password,
    "Nombres":datosC[2],
    "ID":id_user
    }) #agrego los datos del nuevo usuario

    doc = open("users.json","w")
    json.dump( dat_doc,doc,indent=4, separators=(',',': '))
    doc.close()
    print('\n''Usuario creado con exito','\n')
    return '2'

def dat_login(datosS,dat_doc):
    '''
    Se valida la informacion ingresada por el usuario/se retorna la siquiente opcion del menu/Ingresa los datos del usuario el json con todos los usuarios registrados
    '''
    ID="0"
    op='0'
    for user in dat_doc["Users"]:#se recorre la matriz de usuarios
        if datosS[0] == user["Username"] and datosS[1] == decryp(user["Password"]):# si los 3 datos coninciden
            print("\n","*"*4,'Bienvenido',user["Username"],"¿Qué deseas hacer?","*"*4),"\n"
            op='22'#se envia al menu donde podra ver sis datos
            ID=user["ID"] #Sacamos el Usuario para retornarlo junto con la opcion
            break
        else:#Se le envia a la opcion del posible error
            op='02'
    return ID,op

def dat_items_lst(UID,dat_doc):
    '''
    Se buscan y retorna la lista de items del usuario
    '''
    lst_items=[]
    all_item_lst=dat_doc["data"]
    for item in all_item_lst:#se reccorre toda la lista de usuarios con sus items
        if item ["UID"] == UID: #Se compara el ID del usuario ingresado con los guardados en el json 
            lst_items=item["lst_item"] #Si se encuentra, se guardan los datos a la variable que se retornar
    return lst_items

def dat_item_crea(datosMod,lst_items,dat_doc,UID):
    '''
    Creacion de nuevo Item/Se retorna la siguiente opcion/ ingresa los datos digitados del usuario y los datos de usuario
    '''
    if datosMod != []: #Se verifica si es una matriz diligenciada por el usuario esta vacia
        password=cryp(datosMod[2])#Se encripta el password
        IID=str(round(time.time()*1000))[-4:] #Genero un numero aleatorio para el id del item
        if lst_items!=[]: # Verifico si la lista de datos extraida del json contiene informacion
            lst_items.append({ #Agrego los datos diligenciados al diccionario que hira al json
                        "Titulo": datosMod[0],#se guarda el dato que viene de la matriz de datos escritros por el usuario en el diccionario
                        "Username": datosMod[1],
                        "Password": password,
                        "URL": datosMod[3],
                        "Comentario": datosMod[4],
                        "Tags": datosMod[5],
                        "IID": IID
                })
        else: #Si el usuario no tiene lista en el json con los items ingresa aqui
            dat_doc["data"].append({# agrego la lista 
                "UID":UID,#Se crea el usuario dentro del json de items
                "lst_tag":["Sin Tags"], #Se asignan valores por defecto
                "lst_item":[# Agrego los datos diligenciados por el usuario
                    {
                        "Titulo": datosMod[0],#se guarda el dato que viene de la matriz de datos escritros por el usuario en el diccionario
                        "Username": datosMod[1],
                        "Password": password,
                        "URL": datosMod[3],
                        "Comentario": datosMod[4],
                        "Tags": datosMod[5],
                        "IID": IID
                    }
                ]
            })
        doc=open("data.json","w") #Escribo el json
        json.dump( dat_doc,doc,indent=4, separators=(',',': '))
        doc.close()
        print('\n','Item Creado con exito','\n')
    return '1'

def dat_item_search(IID,lst_items):#4
    '''
    Se buscan y retorna un item del usuario
    '''
    item_info=[]#matriz que albergara los datos del item
    for item in lst_items:
        if item ["IID"] == IID: #Se compara el ID del item ingresado con los guardados en el json 
            item_info=item #Si se encuentra, se guardan los datos del item a la variable que se retornara
    return item_info

def dat_item_modi(item_info,dat_doc,datosMod):
    '''
    Se modififican los datos/Se retorna opcion del menu/ Ingresa los datos que modificaran el item y el los datos del item a modificar
    '''
    if datosMod != []: #Se verifica si es una matriz diligenciada por el usuario esta vacia
            password=cryp(datosMod[2])#Se envia a la funcion para desencriptar el dato
            item_info["Titulo"]= datosMod[0]#se guarda el dato de la pocicion n en el diccionario
            item_info["Username"]= datosMod[1]
            item_info["Password"]= password
            item_info["URL"]=datosMod[3]
            item_info["Comentario"]= datosMod[4]
            item_info["Tags"]= datosMod[5]
            doc=open("data.json","w") #Escribo el json
            json.dump( dat_doc,doc,indent=4, separators=(',',': '))#se escribe el json
            doc.close()
            print('\n','Item modificado con exito','\n')
            return '1'
    return '0'

def dat_item_rmv(IID,dat_doc,lst_items):
    '''
    Se modififican elimina un item/Se retorna opcion del menu/ Ingresa el ID del item y la lista del items del usuario
    '''
    for item in lst_items:
        if item ["IID"] == IID: #Se compara el ID del item ingresado con los guardados en el json 
            lst_items.remove(item)
            doc=open("data.json","w") #Escribo el json
            json.dump( dat_doc,doc,indent=4, separators=(',',': '))
            doc.close()#se cierra el archivo
            print('\n','Item eliminado con exito','\n')
            return '1'
    return '0'

def dat_tag_lst(UID,dat_doc):#4
    '''
    Se buscan y retorna la lista de tags del usuario
    '''
    lst_tags=[]#matriz que albergara la lista de un usuario
    all_tags_lst=dat_doc["data"]#matriz que alverga la lista de todos los usuario
    for tag in all_tags_lst:#recorro cada dato de la lista de 
        if tag ["UID"] == UID: #Se compara el ID del usuario ingresado con los guardados en el json 
            lst_tags=tag["lst_tag"] #Si se encuentra, se guardan los datos a la variable que se retornara
    return lst_tags

def dat_tag_search(tagToMod,lst_tags):#4
    '''
    Se buscan el tag en la lista de tags/retorna el tag si lo encuentra
    '''
    tag_found=[]
    for tag in lst_tags:
        if tag == tagToMod: #Se compara para saber si el tag ya existe
            tag_found=tag #Si se encuentra, se guardan los datos a la variable que se retornara
    return tag_found

def dat_tag_crea(tagToMod,lst_tags,dat_doc,UID,opin,lst_items_whitTag):
    '''
    Creacion de nuevo Tag/Se retorna la siguiente opcion/ ingresa los datos digitados del usuario, la lista de tags, los datos de usuario, el id del usuario, la opcion que define el comportamiento de la funcion y la lista de tags dentro del item si se desea modificar
    '''
    if lst_tags !=[]: # Si la lilsta  vacia significa que el usuario no esta en el json de "data" por lo que no tiene ni tags ni items
        if lst_tags[0]!="Sin Tags": #Si la primera posicion no es la que esta por defecto, agregamos el tag sin modificar los anteriores
            lst_tags.append(tagToMod)
            if opin==1:
                for item in lst_items_whitTag:
                    if item["Tags"][0]!="Sin Tags":
                        item["Tags"].append(tagToMod)
                    else:
                        item["Tags"][0]=tagToMod 
        else: # si es la que esta por defecto significa que el usuario ya tiene items pero no a creado ningun tag por la que lo remplazamos 
            lst_tags[0]=tagToMod
    else:
        dat_doc["data"].append({# agrego la lista 
                "UID":UID,#Se crea el usuario dentro del json de items
                "lst_tag":[tagToMod], #Se asignan valores por defecto
                "lst_item":[]
            })
    doc=open("data.json","w") #Escribo el json
    json.dump( dat_doc,doc,indent=4, separators=(',',': '))#se llena el json con los datos
    doc.close()
    print('\n','Tag Creado con exito','\n')
    return '2'

def dat_tag_rmv(tagToMod,lst_tags,dat_doc,lst_items_whitTag):
    '''
    Eliminacion de Tag/Se retorna la siguiente opcion/ ingresa los datos digitados del usuario, la lista de tags, los datos de usuario y la lista de tags del item si se desea modificar un item
    '''
    if lst_tags !=[] and tagToMod !=[] : # Si la lilsta llega vacia significa que no se encontro el tag
        lst_tags.remove(tagToMod) #se eleimina el tag ingresado de la lista
        for item in lst_items_whitTag:
            item["Tags"].remove(tagToMod)
            if len(item["Tags"])==0:#Si la lista quedo vacia, agregamos el tag por defecto
                item["Tags"].append("Sin Tags")
        if len(lst_tags)==0:#Si la lista quedo vacia, agregamos el tag por defecto
            lst_tags.append("Sin Tags")
        doc=open("data.json","w") #Escribo el json
        json.dump( dat_doc,doc,indent=4, separators=(',',': '))
        doc.close()#se cierra el archivo
        print('\n','Tag removido con exito','\n')
        return '2'
    else:
        return '022'

def dat_tag_InItem(tagToMod,lst_items):
    '''
    Busqueda del tag dentro de los items del usuario/ingesea el tag de la lista de tags y la lista de items/retorna lista de los items que tienen el tag
    '''
    lst_items_whitTag=[]
    for item in lst_items: #Busco el item en la lista de items
        for tag in item["Tags"]: #Busco el tag en la lista de tags dentro del item
            if tag == tagToMod: #Comparo el tag que ingresa con el de la lista
                lst_items_whitTag.append(item) # Si es igual retorno todo el item
    return  lst_items_whitTag