from cryptography.fernet import Fernet 

def generateKey():
    '''
    Se genera archivo con la llave 'Primer uso'
    '''
    key = Fernet.generate_key()
    with open("keyUsers.key", "wb") as key_file:
        key_file.write(key)

def loadKey():
    '''
    Se carga la llave del archivo 
    '''
    doc=open("keyUsers.key", "rb").read()#se guarda la llave en un dato a retornar
    return doc

def cryp(password):
    '''
    Se encripta el password del usuario/Ingresa string de password/retorna el password codificado en string
    '''
    keyU=loadKey() #Se llama la funcion para cargar la llave
    menssage=password.encode()#se pasa el string a a bytes
    menssage_ins = Fernet(keyU) # Creamos la instancia de Fernet con la llave que creamos
    menssage_token = menssage_ins.encrypt(menssage) # Encriptamos el mensaje utilizando el método "encrypt"
    pass_cryp_str=menssage_token.decode("UTF-8")#se pasa a string
    return pass_cryp_str #Retornamos el password encriptado

def decryp(pass_cryp_str):
    '''
    Se desencripta el password del usuario/Ingresa el password codificado string /retorna string de password
    '''
    menssage_token=pass_cryp_str.encode()#se pasa el string a a bytes
    keyU=loadKey()#se carga la llave
    menssage_ins = Fernet(keyU)  # Creamos la instancia de Fernet con la llave que creamos
    menssage_decryp= menssage_ins.decrypt(menssage_token) #Desencriptamos el dato
    password=menssage_decryp.decode("UTF-8")#se pasa a string
    return password

